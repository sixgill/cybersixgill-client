from sixgill.sixgill_feed_client import SixgillFeedClient
from sixgill.sixgill_alert_client import SixgillAlertClient
from sixgill.sixgill_constants import FeedStream

CLIENT_ID = "<Replace with your client id>"
CLIENT_SECRET = "<Replace with your client secret>"
CHANNEL_ID = "<Replace with channel id>"


def main():
    sixgill_darkfeed_client = SixgillFeedClient(CLIENT_ID, CLIENT_SECRET, CHANNEL_ID, FeedStream.DARKFEED)

    for indicator in sixgill_darkfeed_client.get_indicator():
        print(indicator)

    sixgill_alert_client = SixgillAlertClient(CLIENT_ID, CLIENT_SECRET, CHANNEL_ID)

    for alert in sixgill_alert_client.get_alert():
        sixgill_alert_client.mark_digested_item(alert)
        print(alert)


if __name__ == "__main__":
    main()
